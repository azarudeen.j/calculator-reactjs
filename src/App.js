
import './App.css';

import React from "react";

import Buttons from "./components/Buttons";
import "../src/index.css"


class App extends React.Component {
  constructor() {
    super();
    this.state = {
      result: ""
    }
  }

  render() {
    return (
      <div className="box">  
        <Buttons name={this.state.result} />
      </div>
    )
  }
}

export default App;

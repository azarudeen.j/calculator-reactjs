import { render } from "@testing-library/react";
import React from "react";
import "../index.css"
import"../App"


class Display extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            result: this.props.name
        }
    }
    
    render() {
        return (
            <div className="display-box">
               {this.props.name}
            </div>
        )
    }
}

export default Display;
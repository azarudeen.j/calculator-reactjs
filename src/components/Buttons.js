import React from "react";
import "../index.css"

import Display from "./Display";


class Buttons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            result: this.props.name
        }
    }


    CreateResult = () => {
        this.setState(prevState => ({
            result: ""+eval(prevState.result)
        }));
    }

    reset = () => {
        this.setState({
            result: ""
        })
    }

    remove = () => {
        this.setState(prevState => ({
            result: prevState.result.substr(0, prevState.result.length-1)
        }));
        console.log(typeof this.state.result)
    }

    combine = (event) => {
        this.setState(prevState => ({
            result: prevState.result + event.target.name
        }));
    }

    handleClickEvent = (event) => {
        console.log(event.target)

        if (event.target.name === "=") {
            this.CreateResult();
        } else if (event.target.name === "C") {
            this.reset();
        } else if (event.target.name === "CE") {
            this.remove();
        } else {
            this.combine(event);
        }

    }

    render() {
        console.log(this.state.result)
        return (

            <div>
                {<Display name={this.state.result} />}
                <button name="(" onClick={this.handleClickEvent}>(</button>
                <button name=")" onClick={this.handleClickEvent}>)</button>
                <button name="CE" onClick={this.handleClickEvent}>CE</button>
                <button name="C" onClick={this.handleClickEvent}>C</button>

                <button name="7" onClick={this.handleClickEvent}>7</button>
                <button name="8" onClick={this.handleClickEvent}>8</button>
                <button name="9" onClick={this.handleClickEvent}>9</button>
                <button name="+" onClick={this.handleClickEvent}>+</button>

                <button name="4" onClick={this.handleClickEvent}>4</button>
                <button name="5" onClick={this.handleClickEvent}>5</button>
                <button name="6" onClick={this.handleClickEvent}>6</button>
                <button name="-" onClick={this.handleClickEvent}>-</button>

                <button name="1" onClick={this.handleClickEvent}>1</button>
                <button name="2" onClick={this.handleClickEvent}>2</button>
                <button name="3" onClick={this.handleClickEvent}>3</button>
                <button name="*" onClick={this.handleClickEvent}>*</button>

                <button name="." onClick={this.handleClickEvent}>.</button>
                <button name="0" onClick={this.handleClickEvent}>0</button>
                <button name="=" onClick={this.handleClickEvent}>=</button>
                <button name="/" onClick={this.handleClickEvent}>/</button>
            </div>
        )
    }
}

export default Buttons;